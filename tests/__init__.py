try:
    from trytond.modules.electrans_visit.tests.test_bank import suite
except ImportError:
    from .test_electrans_visit import suite

__all__ = ['suite']