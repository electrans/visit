# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import visit


def register():
    Pool.register(
        visit.Visit,
        visit.VisitCard,
        visit.VisitCheckOutStart,
        visit.VisitConfiguration,
        visit.VisitConfigurationSequence,
        module='electrans_visit', type_='model')

    Pool.register(
        visit.VisitCheckOut,
        module='electrans_visit', type_='wizard')
