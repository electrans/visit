# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import datetime
from trytond.model import ModelView, ModelSQL, fields, Check, DeactivableMixin
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Bool, If
from trytond.wizard import Wizard, StateTransition, StateView, Button
from trytond.model import ModelView, ModelSQL, ModelSingleton, fields, Workflow
from trytond.modules.company.model import CompanyValueMixin, CompanyMultiValueMixin
from trytond.exceptions import UserError
from trytond.i18n import gettext
from sql import Null
from pytz import timezone

STATES = {'readonly': Eval('state') != 'draft'}
_DEPENDS = ['state']


class VisitCard(DeactivableMixin, ModelSQL, ModelView):
    'Visit Card'
    __name__ = 'visit.card'
    _rec_name = "number"

    number = fields.Char(
        "Number",
        size=None,
        readonly=True)
    being_used = fields.Function(
        fields.Boolean("Being used"),
        'get_being_used',
        searcher='search_being_used')

    def get_being_used(self, name):
        pool = Pool()
        Visit = pool.get('visit.visit')
        if Visit.get_active_visits(self.id):
            return True

    @classmethod
    def search_being_used(cls, name, clause):
        pool = Pool()
        Visit = pool.get('visit.visit')
        aux = 'in' if clause[2] else 'not in'
        cards = [v.card.id for v in Visit.get_active_visits() if v.card]
        return [('id', aux, cards)]

    @classmethod
    def create(cls, vlist):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('visit.configuration')
        config = Config(1)

        vlist = [x.copy() for x in vlist]
        for values in vlist:
            if values.get('number') is None:
                values['number'] = Sequence.get_id(
                    config.visiting_card_sequence.id)
        return super(VisitCard, cls).create(vlist)

    @classmethod
    def write(cls, *args):
        actions = iter(args)
        for visits, values in zip(actions, actions):
            if 'active' in values and not values['active']:
                if [visit for visit in visits if visit.being_used]:
                    raise UserError(gettext('electrans_visit.card_in_use'))
        super(VisitCard, cls).write(*args)


class Visit(Workflow, ModelSQL, ModelView):
    'Visit'
    __name__ = 'visit.visit'

    party = fields.Many2One(
        'party.party', "Party",
        states=STATES,
        depends=_DEPENDS)
    name = fields.Char(
        "Name",
        states=STATES,
        depends=_DEPENDS)
    start = fields.DateTime(
        "Start",
        states=STATES,
        depends=_DEPENDS)
    end = fields.DateTime(
        "End",
        states={
            'readonly': Eval('state') != 'visiting'},
        depends=_DEPENDS)
    card = fields.Many2One(
        'visit.card', "Card",
        domain=[If(Eval('state') == 'draft',
                ('being_used', '=', False),
                ())],
        states={
            'invisible': True},
        depends=_DEPENDS)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('visiting', 'Visiting'),
        ('done', 'Done')],
        "State", readonly=True)
    employee = fields.Many2One(
        'company.employee', "Employee",
        states=STATES,
        depends=_DEPENDS)
    # datetime fields as a string because is not allowed to use the datetime widget in the tree
    str_start = fields.Function(
        fields.Char("Start"),
        'get_str_start')
    str_end = fields.Function(
        fields.Char("End"),
        'get_str_end')

    @classmethod
    def __setup__(cls):
        super(Visit, cls).__setup__()
        cls._transitions |= set((
                ('draft', 'visiting'),
                ('visiting', 'draft'),
                ('visiting', 'done')
                ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state') != 'visiting',
                'depends': ['state'],
                'icon': 'tryton-go-previous'},
            'visiting': {
                'invisible': Eval('state') != 'draft',
                'depends': ['state'],
                'icon': 'tryton-go-next'},
            'done': {
                'invisible': Eval('state') != 'visiting',
                'depends': ['state'],
                'icon': 'tryton-go-next'}})

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default.setdefault('card', None)
        default.setdefault('start', None)
        default.setdefault('end', None)
        return super(Visit, cls).copy(records, default=default)

    @classmethod
    def delete(cls, visits):
        for visit in visits:
            if visit.state != 'draft':
                raise UserError(gettext('electrans_visit.delete_visit_draft'))
        super(Visit, cls).delete(visits)

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, visits):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('visiting')
    def visiting(cls, visits):
        visits = [visit for visit in visits if not visit.start]
        cls.write(visits, {'start': datetime.datetime.today()})

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, visits):
        visits = [visit for visit in visits if not visit.end]
        cls.write(visits, {'end': datetime.datetime.today()})

    @classmethod
    def get_active_visits(cls, card_id=None):
        pool = Pool()
        Visit = pool.get('visit.visit')
        domain = [('state', '=', 'visiting')]
        if card_id:
            domain += [('card', '=', card_id)]
        return Visit.search(domain)

    def get_str_start(self, name):
        # convert Coordinated Universal Time (UTC) to 'Europe/Madrid' datetime
        return self.start.astimezone(timezone('Europe/Madrid')).strftime("%d/%m/%Y, %H:%M") if self.start else ''

    @staticmethod
    def order_str_start(tables):
        table, _ = tables[None]
        return [table.start == Null, table.start]

    def get_str_end(self, name):
        # convert Coordinated Universal Time (UTC) to 'Europe/Madrid' datetime
        return self.end.astimezone(timezone('Europe/Madrid')).strftime("%d/%m/%Y, %H:%M") if self.end else ''

    @staticmethod
    def order_str_end(tables):
        table, _ = tables[None]
        return [table.end == Null, table.end]


class VisitCheckOut(Wizard):
    'Visit Check Out'
    __name__ = 'visit.check_out'

    start = StateView('visit.check_out.start',
                      'electrans_visit.visit_check_out_start_view_form', [
                          Button('Cancel', 'end', 'tryton-cancel'),
                          Button('Confirm', 'confirm', 'tryton-ok', default=True)])
    confirm = StateTransition()

    def transition_confirm(self):
        pool = Pool()
        Visit = pool.get('visit.visit')
        card = self.start.card
        if card:
            visit = Visit.get_active_visits(card)
            if visit:
                visit = visit[0]
                if visit.state in ['draft', 'visiting']:
                    if self.start.end:
                        visit.end = self.start.end
                        visit.save()
                    Visit.visiting([visit])
                    Visit.done([visit])
        else:
            raise UserError(gettext('electrans_visit.invalid_card_number'))
        return 'end'

    def default_start(self, fields):
        return {'end': datetime.datetime.today()}


class VisitCheckOutStart(ModelView):
    'Visit Check Out Start'
    __name__ = 'visit.check_out.start'

    card = fields.Many2One(
        'visit.card', "Visiting Card",
        domain=[('being_used', '=', True)])
    end = fields.DateTime('End')


def default_func(field_name):
    @classmethod
    def default(cls, **pattern):
        return getattr(
            cls.multivalue_model(field_name),
            'default_%s' % field_name, lambda: None)()
    return default


class VisitConfiguration(ModelSingleton, ModelSQL, ModelView, CompanyMultiValueMixin):
    'Visit Configuration'
    __name__ = 'visit.configuration'

    visiting_card_sequence = fields.MultiValue(fields.Many2One(
            'ir.sequence', "Visiting Card Sequence", required=True,
            domain=[
                ('company', 'in',
                    [Eval('context', {}).get('company', -1), None]),
                ('code', '=', 'visit.card'),
                ]))

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field == 'visiting_card_sequence':
            return pool.get('visit.configuration.sequence')
        return super(VisitConfiguration, cls).multivalue_model(field)

    default_visiting_card_sequence = default_func('visiting_card_sequence')


class VisitConfigurationSequence(ModelSQL, CompanyValueMixin):
    "Visit Configuration Sequence"
    __name__ = 'visit.configuration.sequence'

    visiting_card_sequence = fields.Many2One(
        'ir.sequence', "Visiting Card Sequence", required=True,
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'visit.card'),
            ],
        depends=['company'])

    @classmethod
    def default_visiting_card_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('electrans_visit', 'sequence_visiting_card')
        except KeyError:
            return None
